#!/bin/bash

BASE_URL="https://www.steamgriddb.com/api/v2"

NAME="$1"

[ -z "$NAME" ] && echo "no game name given" && exit
ID="$(curl -s -H "Authorization: Bearer dffa15c4f323227b9072aa8cb1c00538" "${BASE_URL}/search/autocomplete/${NAME}" | jq -r '.data[0] | .id')"
NAME="$(curl -s -H "Authorization: Bearer dffa15c4f323227b9072aa8cb1c00538" "${BASE_URL}/search/autocomplete/${NAME}" | jq -r '.data[0] | .name')"
NAME="$(echo $1 | tr '[:upper:]' '[:lower:]')"
NAME="${NAME// /-}"
GRID="$(curl -s -H "Authorization: Bearer dffa15c4f323227b9072aa8cb1c00538" "${BASE_URL}/grids/game/${ID}?dimensions=920x430" | jq -r '.data | max_by(.upvotes) | .url')"
# [ -e "./cache/$NAME" ] || mkdir -p ./cache/$NAME
[ -e "./games" ] || mkdir -p ./games
cd games
curl -s "$GRID" -o "$NAME".png
cd ..
